#include <cstdint>
#include <cstddef>
#include <string>

#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/uart.h"
#include "driverlib/pwm.h"
#include "driverlib/fpu.h"
#include "driverlib/qei.h"
#include "uartstdio.h"

#define LED_RED GPIO_PIN_1
#define LED_BLUE GPIO_PIN_2
#define LED_GREEN GPIO_PIN_3

extern "C" {
int main();
}

void configureUART(void) {
	//
	// Enable the GPIO Peripheral used by the UART.
	//
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

	//
	// Enable UART0
	//
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

	//
	// Configure GPIO Pins for UART mode.
	//
	ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

	//
	// Use the internal 16MHz oscillator as the UART clock source.
	//
	UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

	//
	// Initialize the UART for console I/O.
	//
	UARTStdioConfig(0, 115200, 16000000);
}

int main() {
	// enable hardware floating point
	ROM_FPULazyStackingEnable();
	ROM_FPUEnable();

	// set system clock to 80Mhz
	ROM_SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);

	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);

	// led lights init
	ROM_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, LED_RED|LED_BLUE|LED_GREEN);

	ROM_GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_5);


	configureUART();

	//Unlock GPIOD7 - Like PF0 its used for NMI - Without this step it doesn't work
	HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY; //In Tiva include this is the same as "_DD" in older versions (0x4C4F434B)
	HWREG(GPIO_PORTD_BASE + GPIO_O_CR) |= 0x80;
	HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = 0;





	//Set Pins to be PHA0 and PHB0
	GPIOPinConfigure(GPIO_PD6_PHA0);
	GPIOPinConfigure(GPIO_PD7_PHB0);

	//Set GPIO pins for QEI. PhA0 -> PD6, PhB0 ->PD7. I believe this sets the pull up and makes them inputs
	GPIOPinTypeQEI(GPIO_PORTD_BASE, GPIO_PIN_6 |  GPIO_PIN_7);



	//
	// Enable the QEI0 peripheral
	//
	SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI0);
	//
	// Wait for the QEI0 module to be ready.
	//
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_QEI0))
	{
	}
	//
	// Configure the quadrature encoder to capture edges on both signals and
	// maintain an absolute position by resetting on index pulses. Using a
	// 1000 line encoder at four edges per line, there are 4000 pulses per
	// revolution; therefore set the maximum position to 3999 as the count
	// is zero based.
	//
	// QEIConfigure(QEI0_BASE, (QEI_CONFIG_CAPTURE_A | QEI_CONFIG_NO_RESET |
	// 	QEI_CONFIG_QUADRATURE | QEI_CONFIG_NO_SWAP), 3999);
	
	// QEIConfigure(QEI0_BASE, (QEI_CONFIG_CAPTURE_A | QEI_CONFIG_NO_RESET |
	// 	QEI_CONFIG_CLOCK_DIR | QEI_CONFIG_NO_SWAP), 1000);



	//DISable peripheral and int before configuration
	QEIDisable(QEI0_BASE);
	QEIIntDisable(QEI0_BASE,QEI_INTERROR | QEI_INTDIR | QEI_INTTIMER | QEI_INTINDEX);

	QEIConfigure(QEI0_BASE, (QEI_CONFIG_CAPTURE_A_B | QEI_CONFIG_NO_RESET |
		QEI_CONFIG_QUADRATURE | QEI_CONFIG_NO_SWAP), 1000000);



	QEIFilterConfigure(QEI0_BASE, QEI_FILTCNT_2);
	// QEIFilterDisable(QEI0_BASE);
	QEIFilterEnable(QEI0_BASE);




	QEIVelocityConfigure(QEI0_BASE, QEI_VELDIV_2, 10);
	QEIVelocityEnable(QEI0_BASE);



	//
	// Enable the quadrature encoder.
	//
	QEIEnable(QEI0_BASE);


	// QEIPositionSet(QEI0_BASE, 1000);





	// flash LED
	while(1) {
		ROM_GPIOPinWrite(GPIO_PORTF_BASE, LED_RED|LED_GREEN|LED_BLUE, LED_GREEN);
		ROM_SysCtlDelay(4000000);
		ROM_GPIOPinWrite(GPIO_PORTF_BASE, LED_RED|LED_GREEN|LED_BLUE, 0);
		ROM_SysCtlDelay(4000000);

		uint32_t pos = QEIPositionGet(QEI0_BASE);
		int dir = QEIDirectionGet(QEI0_BASE);
		bool err = QEIErrorGet(QEI0_BASE);
		int32_t pins = GPIOPinRead(GPIO_PORTC_BASE, GPIO_PIN_5);
		int32_t vel = QEIVelocityGet(QEI0_BASE);
		UARTprintf("pos: %d\tdir: %d\terr: %d\tvel: %d\n", pos, dir, err, vel);
	}
}

